import React, { Component } from "react";
import { BrowserRouter, Route, Link, Routes  } from "react-router-dom";
import "./App.css";
import AddTutorial from "./components/add-tutorial.component";
import Tutorial from "./components/tutorial.component";
import TutorialsList from "./components/tutorials-list.component";

class App extends Component {

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
    console.warn(error, errorInfo);
  }
  render() {
    return (
      <BrowserRouter>
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <Link to={"/tutorials"} className="navbar-brand">
            NERMS
          </Link>
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/tutorials"} className="nav-link">
                Tutorials
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/add"} className="nav-link">
                Add
              </Link>
            </li>
          </div>
        </nav>

        <div className="container mt-3">
          <Routes>
              <Route path={"/"} element={<TutorialsList />} />
              <Route path={"/tutorials"} element={<TutorialsList />} />
              <Route path="/add" element={<AddTutorial />} />
              <Route path="/tutorials/:id" element={<Tutorial />} />
          </Routes>

        </div>
      </div>
      </BrowserRouter>
    );
  }
}

export default App;
