# nerms

## DEPLOY
1) Adjust .env files to suit your needs.
2) Docker-compose up -d
3) Enjoy

## Local Test
1) Have MYSQL running in either docker or local, match .env
2) Npm install frontend and backend
3) npm start in both
